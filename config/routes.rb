Rails.application.routes.draw do
  resources :users
  resources :tasks
  resources :user_mailer
  # get 'home/hello'
  # get 'home/goodbye'
  get 'tasks/new'
  get 'users/new'
  post "sign_up", to: "users#create"
  get "sign_up", to: "users#new"
  get "confirmation", to: "user_mailer#confirmation"
  post "login", to: "sessions#create"
  delete "logout", to: "sessions#destroy"
  get "login", to: "sessions#new"
  put "account", to: "users#update"
  get "account", to: "users#edit"
  delete "account", to: "users#destroy"
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  root "users#new"
  resources :confirmations, only: [:create, :edit, :new], param: :confirmation_token
  resources :passwords, only: [:create, :edit, :new, :update], param: :password_reset_token
  resources :active_sessions, only: [:destroy] do
    collection do
      delete "destroy_all"
    end
  end
end
