# frozen_string_literal: true

class FormInputComponent < ViewComponent::Base
  def initialize(id:)
    @id = id
  end
end
